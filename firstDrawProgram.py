#first draw house

import turtle
turtle.speed('normal')
length = 100
degrees = 90

wn = turtle.Screen()
turtle.forward(length)
turtle.backward(length)
turtle.left(degrees)
turtle.color('red')
turtle.forward(length)
turtle.right(degrees)
turtle.color('blue')
turtle.forward(length)
turtle.right(degrees)
turtle.color('green')
turtle.forward(length)
turtle.backward(length)
degreesMid = 150
turtle.right(degreesMid)
turtle.color('brown')
turtle.forward(100)
turtle.left(degreesMid-30)
turtle.forward(100)
turtle.left(degreesMid-30)
turtle.color('blue')
turtle.forward(50)
turtle.left(90)
turtle.color('yellow')
turtle.forward(85)

turtle.color('blue')
turtle.goto(0, 100)

#second draw

i = 10
turtle.speed('fastest')

while True:
    turtle.left(i % 48)
    turtle.forward(10)
    i += 1
