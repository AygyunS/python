#exercises: remove highest and lowest price and find average price
priceList = []

print("If you want to stop entering prices you just have to enter \"stop\"")

while True:
    currentPrice = input("Enter price: ")
    if currentPrice == 'stop':
        break
    else:
        priceList.append(float(currentPrice))

print(priceList)

priceList.sort()

mn = min(priceList)
while len(priceList) > 0 and priceList[0] == mn:
    priceList.pop(0)

mx = max(priceList)
while len(priceList) > 0 and priceList[-1] == mx:
    priceList.pop(-1)

print(priceList)
s = sum(priceList)
sr = s/len(priceList)
print(sr)


