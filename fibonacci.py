def fibonacci(max):
    a, b = 0, 1
    while a < max:
        yield a
        a, b = b, a+b

maxNum = int(input("Enter max range of fibonacci: "))
for n in fibonacci(maxNum):
    print(n, end=" ")
