#Basic set exc.
setRange = int(input("Enter range of set, just some number: "))
set1 = set((range(setRange)))
set2 = set((range(5, 16)))

print("Set 1: ", set1)
print("Set 2: ", set2)

print(set1.intersection(set2))

addN = int(input("Enter number to set1: "))
set1.add(addN)
print("Set1 with added number: ", set1)
remN = int(input("Remove some number of set1: "))
set1.remove(remN)
print("Set1 without removed number: ", set1)

search_and_find_N = int(input("Enter number from set1 to print only this number: "))
if search_and_find_N in set1:
    print("This number is in set1 !")
else:
    print("This number is not in set1 !")
