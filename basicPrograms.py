#Loops(for) and array
names = ["Pesho", "Ivan", "Niki"]

for i in range(len(names)):
    name = names[i]
    print('Hello ' + name + '! at index ' + str(i))


#Loop(while)
n = 6
fact = 1

while n:
    fact *= n
    print(str(fact) + " ")
    n -= 1


#Array
n = list()
num = input("Enter how much elements you want: ")
print("Enter numbers in array: ")
for i in range(int(num)):
    n1 = input("num: ")
    n.append(int(n1))
print("Array elements: ", n)

#Array1
n=10
l = [n for i in range(n)]
for i in range(len(l)):
    print(i, end=" ")

#Array-List
from random import randint

row = 3
col = 8
if row > col:
    randomNum = randint(0, row)
else:
    randomNum = randint(0, col)

a = [["3" for i in range(row)] for j in range(col)]

for i in range(len(a)):
  print()
  for j in a[i]:
     print("%c " %j, end='')

#Array2
import random
number = list(range(10))
random.shuffle(number)

matrix = [[[cell for cell in range(3)]
           for col in range(5)]
                for row in range(10)]
print(matrix)


#Set
class Person:
    """docstring for Persen"""
    def __init__(self, name):
        self.name = name
    def __repr__(self):
        return self.name
    def __eq__(self, other):
        return self.name is other.name
    def __hash__(self):
        return self.name.__hash__()


people = {Person("Niki")}
people = {Person("Niki")}
print(people)

people.add(Person("Ico"))
print(people)



#Dictionaries
musicPreferences = {
  "Rock": ["Peter", "Georgi"],
  "Pop Folk": ["Maria", "Teodor"],
  "Blues and Jazz": ["Alex", "John"],
  "Pop": ["Nikol", "Ford", "Elena"]
}


#Function
def sum(numbers):
    sum = 0
    for num in numbers:
        sum += num
    return sum

def printMes(message):
    print("Message: {0}".format(message))

print(sum(list(range(1, 11))))
printMes("Hello!")


#Function1
def sum(numbers):
    sum = 0
    for num in numbers:
        sum += num
    return sum

def printMes(message):
    print("Message: {0}".format(message))

print(sum(list(range(1, 11))))
printMes("Hello!")


