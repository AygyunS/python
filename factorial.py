def factorial(factNum):
    if factNum == 0:
        return 1

    n = factNum * factorial(factNum - 1)
    print(n, end=' ')
    return n

factNum = int(input("Enter N!: "))
factorial(factNum)
